import PokemonCard from './PokemonCard'
import React, { Component } from 'react'
import axios from 'axios'
import '../asset/css/index.css'

class PokemonList extends Component {
    constructor() {
        super()
        this.loadOnScroll = this.loadOnScroll.bind(this)
        this.myRef = React.createRef()
        this.state = {
            isLoading: false,
            nextUrl: "https://pokeapi.co/api/v2/pokemon?limit=40",
            pokemons: []
        }
    }

    componentDidMount() {
        this.loadPokemons()
    }

    render() {
        return (
            <div className="pokemon-list" onScroll={(e) => this.loadOnScroll(e)} ref={this.myRef}>
                {this.state.pokemons.length > 0 && this.state.pokemons.map((pokemon) => (
                    <PokemonCard key={pokemon.name} name={pokemon.name} url={pokemon.url} />
                ))}
                <div>
                    <button className="show-details" disabled>loading...</button>
                </div>
            </div>
        )
    }

    loadOnScroll(e) {
        if (e.target.scrollTop + e.target.clientHeight > e.target.scrollHeight - 20) {
            this.loadPokemons()
        }
    }

    loadPokemons() {
        if (!this.state.isLoading && this.state.nextUrl != null) {
            this.setState({ isLoading: true })
            axios.get(this.state.nextUrl)
                .then((response) => {
                    this.setState({
                        isLoading: false,
                        nextUrl: response.data.next,
                        pokemons: this.state.pokemons.concat(response.data.results)
                    })
                })
                .catch((error) => {
                    this.setState({ error: error })
                    console.log(error)
                })
        }
    }
}

export default PokemonList
