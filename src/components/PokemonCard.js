import React, { Component, div, Fragment } from 'react'
import axios from 'axios'
import Modal from 'react-modal'
import '../asset/css/index.css'


export class PokemonCard extends Component {
    constructor() {
        super()
        this.showDetails = this.showDetails.bind(this)
        this.hideDetails = this.hideDetails.bind(this)
        this.state = {
            isDetailsShown: false,
            isDetailsLoaded: false,
            details: null
        }
    }

    render() {
        return (
            <div>
                <button className='show-details' onClick={this.showDetails}>{this.props.name.toUpperCase()}</button>
                {this.state.isDetailsShown && <Modal isOpen={this.state.isDetailsShown}>
                    {this.state.isDetailsLoaded ?
                        (<Fragment>
                            <button className="modal-close" onClick={this.hideDetails}>
                                CLOSE
                            </button>
                            <div className="card-header">
                                <div className="pokemon-sprite">
                                    <img src={this.state.details.sprite} alt={this.props.name + " sprite"} className="pokemon-sprite__img"></img>
                                </div>
                                <div className="pokemon-main-stat">
                                    <h4 className="pokemon-number">#{this.state.details.number}</h4>
                                    <h2 className="pokemon-name">{this.state.details.name.toUpperCase()}</h2>
                                    <ul className="pokemon-stat__list">
                                        <li><strong>Base exp</strong> {this.state.details.baseExp}</li>
                                        <li><strong>Height</strong> {this.state.details.height}</li>
                                        <li><strong>Weight</strong> {this.state.details.weight}</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="card-content">
                                <div className="pokemon-stat">
                                    <h2 className="pokemon-stat__title">Stats</h2>
                                    <ul className="pokemon-stat__list">
                                        {this.state.details.stats.map(stat => <li><strong>{stat.name.split("-").join(" ")}</strong> {stat.baseStat}</li>)}
                                    </ul>
                                </div>
                                <div className="pokemon-stat">
                                    <h2 className="pokemon-stat__title">Types</h2>
                                    <ul className="pokemon-stat__list">
                                        {this.state.details.types.map(type => <li><strong>{type.split("-").join(" ")}</strong></li>)}
                                    </ul>
                                </div>
                                <div className="pokemon-stat">
                                    <h2 className="pokemon-stat__title">Abilities</h2>
                                    <ul className="pokemon-stat__list">
                                        {this.state.details.abilities.map(ability => <li><strong>{ability.split("-").join(" ")}</strong></li>)}
                                    </ul>
                                </div>
                                <div className="pokemon-stat">
                                    <h2 className="pokemon-stat__title">Moves</h2>
                                    <ul className="pokemon-stat__list">
                                        {this.state.details.moves.map(move => <li><strong>{move.split("-").join(" ")}</strong></li>)}
                                    </ul>
                                </div>
                            </div>
                        </Fragment>)
                        : ((<p>loading...</p>) && this.loadDetails(this.props.url))
                    }
                </Modal>}
            </div>
        )
    }

    showDetails() {
        this.setState({ isDetailsShown: true })
    }

    hideDetails() {
        this.setState({ isDetailsShown: false })
    }

    loadDetails(url) {
        axios.get(url)
            .then((response) => {
                console.log(response)
                const details = {
                    number: response.data.id,
                    name: response.data.name,
                    sprite: response.data.sprites.front_default,
                    baseExp: response.data.base_experience,
                    weight: response.data.weight,
                    height: response.data.height,
                    stats: response.data.stats.map((obj) => ({ name: obj.stat.name, baseStat: obj.base_stat })),
                    types: response.data.types.map((obj) => obj.type.name),
                    abilities: response.data.abilities.map((obj) => obj.ability.name),
                    moves: response.data.moves.map((obj) => obj.move.name),
                }
                this.setState({ details: details, isDetailsLoaded: true })
            })
            .catch((error) => {
                this.setState({ error: error })
                console.log(error);
            })
    }
}

export default PokemonCard
