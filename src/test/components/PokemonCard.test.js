import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

Enzyme.configure({ adapter: new Adapter() });

import PokemonCard from '../../components/PokemonCard'

describe('Pokemon Card', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallow(<PokemonCard name='bulbasaur' url='https://pokeapi.co/api/v2/pokemon/1/' />)
    })

    it('should hide details by default', () => {
        expect(wrapper.find('.pokemon-details')).toHaveLength(0)
    })

    it('should show details after button has been clicked', () => {
        wrapper.find('.show-details').simulate('click');
        expect(wrapper.find('.pokemon-details')).toHaveLength(1)
    })
})