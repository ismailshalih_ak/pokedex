import PokemonList from './components/PokemonList'
import './asset/css/index.css'

function App() {
  return (
    <div className="App">
      <div className="app-header"><h1>Pok&eacute;dex</h1></div>
      <PokemonList />
    </div>
  );
}

export default App;
